import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { MockedProvider, MockedResponse } from "@apollo/client/testing";
import Home, { QUERY } from "./index";
import "@testing-library/jest-dom";

const mocks: any = [
  {
    request: {
      query: QUERY,
      variables: {
        limit: 100,
      },
    },
    result: () => {
      return {
        data: {
          launches: [
            {
              id: "5eb87d47ffd86e000604b38a",
              launch_date_local: "2020-08-30T19:18:00-04:00",
              mission_name: "SAOCOM 1B, GNOMES-1, Tyvak-0172",
              __typename: "Launch",
            },
            {
              id: "5ef6a1e90059c33cee4a828a",
              launch_date_local: "2020-09-03T08:46:00-04:00",
              mission_name: "Starlink-11 (v1.0)",
              __typename: "Launch",
            },
          ],
        },
      };
    },
  },
];

describe("Home", () => {
  it("renders the table", () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Home />
      </MockedProvider>
    );

    const heading = screen.getByText(/Liste des missions spacex/i);

    expect(heading).toBeInTheDocument();
  });
  it("show the list of mission", async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Home />
      </MockedProvider>
    );

    const missions1 = await screen.findByText(/SAOCOM 1B/i);
    const missions2 = await screen.findByText(/Starlink-11/i);

    expect(missions1).toBeInTheDocument();
    expect(missions2).toBeInTheDocument();
  });
  it("load more items when clicking on the button", async () => {
    render(
      <MockedProvider mocks={mocks} addTypename={false}>
        <Home />
      </MockedProvider>
    );

    const missions1 = await screen.findByText(/SAOCOM 1B/i);

    expect(missions1).toBeInTheDocument();

    const button = await screen.findByRole("button", { name: /charger/i });

    userEvent.click(button);
    const loadingButton = await screen.findByRole("button", {
      name: /chargement/i,
    });
    expect(loadingButton).toBeInTheDocument();
  });
});
